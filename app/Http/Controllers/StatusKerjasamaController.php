<?php

namespace App\Http\Controllers;

use App\Models\DataKerjasama;
use App\Models\StatusKerjasama;
use Illuminate\Http\Request;
use Rap2hpoutre\FastExcel\FastExcel;

class StatusKerjasamaController extends Controller
{
    public function index()
    {
        $status = StatusKerjasama::get();
        return view('referensi.admin-status-mou', compact('status'));
    }

    public function store(Request $request)
    {
        $request->validate([
            'nama' => 'required|string|max:50'
        ]);

        StatusKerjasama::create([
            'nama' => $request->nama
        ]);

        return to_route('status.index')->with('success', 'Status MOU berhasil di tambahkan');
    }

    public function update(Request $request, StatusKerjasama $statusKerjasama)
    {
        $request->validate([
            'nama' => 'required|string|max:50'
        ]);

        $statusKerjasama->update([
            'nama' => $request->nama
        ]);

        return to_route('status.index')->with('success', 'Status MOU berhasil di update');
    }

    public function destroy(StatusKerjasama $statusKerjasama)
    {
        $statusKerjasama->delete();

        return to_route('status.index')->with('success', 'Status MOU berhasil di hapus');
    }

    public function exportStatus()
    {
        $status = DataKerjasama::with('mitra:id,nama_mitra')->select('mitra_id', 'deskripsi', 'status')->get();
        $no = 1;

        (new FastExcel($status))->export('data-status-kerjasama.xlsx', function ($status) use (&$no) {
            return [
                'No' => $no++,
                'Judul Kerjasama' => $status->deskripsi,
                'Status' => $status->status,
            ];
        });

        return response()->download('data-status-kerjasama.xlsx')->deleteFileAfterSend();
    }

    public function show(DataKerjasama $status)
    {
        return view('refrensi.admin-status-mou-show', compact('status'));
    }
}
