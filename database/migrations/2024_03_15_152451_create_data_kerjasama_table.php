<?php

use App\Models\Mitra;
use App\Models\User;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('data_kerjasama', function (Blueprint $table) {
            $table->id();
            $table->date('tanggal_mou');
            $table->date('tanggal_akhir');
            $table->string('deskripsi');
            $table->string('no_surat_instansi')->nullable();
            $table->string('no_surat_mitra')->nullable();
            $table->string('file');
            $table->string('jenis_dokumen')->nullable();
            $table->string('status')->default('Penyusunan Draft');
            $table->foreignIdFor(User::class)->nullable();
            $table->foreignIdFor(Mitra::class);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('data_kerjasama');
    }
};
