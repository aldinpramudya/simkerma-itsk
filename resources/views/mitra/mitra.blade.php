@extends('base')
@section('title', 'SIMKERMA | Mitra')
@section('konten')
<div class="col-md-8">
    <h6 class="page-title">Mitra</h6>
    <ol class="breadcrumb m-0">
        <li class="breadcrumb-item"><a href="#">SIMKERMA</a></li>
        <li class="breadcrumb-item active" aria-current="page">Mitra</li>
    </ol>
</div>
<div class="row align-items-center mb-0">
    <div class="col-md-12">
        <div class="float-end d-md-block">
            <div class="my-3 text-center">
                <button class="btn btn-primary mx-2" type="button" aria-expanded="false" data-bs-toggle="modal" data-bs-target="#myModalTambah">
                    <i class="fa-solid fa-plus me-2"></i> Tambah Mitra Baru
                </button>
                <button class="btn btn-primary" type="button" aria-expanded="false" onclick="window.location.href='/export-mitra'">
                    <i class="mdi mdi-file-export me-2"></i> Ekspor
                </button>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="table-responsive">
                    <table id="datatable" class="table table-bordered table-responsive mb-0 dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                        <thead>
                            <tr>
                                <th>No.</th>
                                <th>Nama Mitra</th>
                                <th>Jenis Mitra</th>
                                <th>Nama PJ</th>
                                <th>Alamat Email</th>
                                <th>Nomor Telepon</th>
                                <th>Edit</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($mitra as $key => $data)
                            <tr>
                                <td>{{ $key + 1 }}</td>
                                <td>{{ $data->nama_mitra }}</td>
                                <td>{{ $data->jenis_mitra }}</td>
                                <td>{{ $data->penanggung_jawab }}</td>
                                <td>{{ $data->email }}</td>
                                <td>{{ $data->no_telp }}</td>
                                <td class="text-center">
                                    <div class="d-flex justify-content-center align-items-center">
                                        <!-- Button untuk mengedit -->
                                        <button type="button" class="btn btn-warning waves-effect waves-light my-0" data-bs-toggle="modal" data-bs-target="#myModalEdit{{ $data->id }}"><i class="fa-solid fa-pencil"></i></button>
                                        <!-- Button untuk menghapus -->
                                        <form id="deleteForm{{ $data->id }}" action="{{ route('mitra.destroy', $data->id) }}" method="POST" class="deleteBtn">
                                            @csrf
                                            @method('DELETE')
                                            <button type="submit" class="btn btn-danger waves-effect waves-light ms-1 me-1">
                                                <i class="fas fa-trash-alt" style="color: white;"></i>
                                            </button>
                                        </form>
                                    </div>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

{{-- Modal Form Add --}}
<div id="myModalTambah" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabelTambah" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <form action="{{ route('mitra.store') }}" method="POST">
                @csrf
                <div class="modal-header">
                    <h5 class="modal-title" id="myModalLabel2">Tambah Mitra Baru</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <div class="col">
                        <div class="col-md-12 pb-3">
                            <label for="nama-mitra" class="form-label">Nama Mitra</label>
                            <input type="text" class="form-control" id="nama-mitra" name="nama_mitra" autocomplete="off" required>
                        </div>
                        <div class="col-md-12 pb-3">
                            <label for="jenis-mitra" class="form-label">Jenis Mitra</label>
                            <select class="form-select" aria-label="Default select example" id="jenis-mitra" name="jenis_mitra" required>
                                @foreach ($jenisMitra as $jenis)
                                <option value="{{ $jenis->nama }}">{{ $jenis->nama }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-md-12 pb-3">
                            <label for="nama-pj" class="form-label">Nama PJ</label>
                            <input type="text" class="form-control" id="nama-pj" name="penanggung_jawab" required>
                        </div>
                        <div class="col-md-12 pb-3">
                            <label for="email-mitra" class="form-label">Alamat Email</label>
                            <input type="email" class="form-control" id="email-mitra" name="email" autocomplete="off" required>
                        </div>
                        <div class="col-md-12 pb-3">
                            <label for="no-telp" class="form-label">Nomor Telepon</label>
                            <input type="text" class="form-control" id="no-telp" name="no_telp" required>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Batal</button>
                    <button type="submit" class="btn btn-primary">Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div>

{{-- Modal Form Edit --}}
@foreach($mitra as $data)
<div id="myModalEdit{{ $data->id }}" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabelEdit{{ $data->id }}" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <form action="{{ route('mitra.update', $data->id) }}" method="POST">
                @csrf
                @method('PUT')
                <div class="modal-header">
                    <h5 class="modal-title" id="myModalLabelEdit{{ $data->id }}">Edit Mitra</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <div class="col">
                        <div class="col-md-12 pb-3">
                            <label for="nama{{ $data->id }}" class="form-label">Nama Mitra</label>
                            <input type="text" class="form-control" id="nama{{ $data->id }}" name="nama_mitra" value="{{ $data->nama_mitra }}" autocomplete="off" required>
                        </div>
                        <div class="col-md-12 pb-3">
                            <label for="jenis{{ $data->id }}" class="form-label">Jenis Mitra</label>
                            <select class="form-select" aria-label="Default select example" id="jenis{{ $data->id }}" name="jenis_mitra" required>
                                @foreach ($jenisMitra as $jenis)
                                <option value="{{ $jenis->nama }}" {{ $data->jenis_mitra == $jenis->nama ? 'selected' : '' }}>
                                    {{ $jenis->nama }}
                                </option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-md-12 pb-3">
                            <label for="nama-pj{{ $data->id }}" class="form-label">Penanggung Jawab</label>
                            <input type="text" class="form-control" id="nama-pj{{ $data->id }}" name="penanggung_jawab" value="{{ $data->penanggung_jawab }}" autocomplete="off" required>
                        </div>
                        <div class="col-md-12 pb-3">
                            <label for="email{{ $data->id }}" class="form-label">Alamat Email</label>
                            <input type="email" class="form-control" id="email{{ $data->id }}" name="email" value="{{ $data->email }}" autocomplete="off" required>
                        </div>
                        <div class="col-md-12 pb-3">
                            <label for="no-telp{{ $data->id }}" class="form-label">Nomor Telepon</label>
                            <input type="text" class="form-control" id="no-telp{{ $data->id }}" name="no_telp" value="{{ $data->no_telp }}" autocomplete="off" required>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary waves-effect" data-bs-dismiss="modal">Batal</button>
                    <button type="submit" class="btn btn-primary waves-effect waves-light">Simpan</button>
                </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
</div>
@endforeach
@endsection

@push('script')
<script>
    $(document).ready(function() {
        $(".deleteBtn").click(function(event) {
            event.preventDefault(); // Prevent default form submission

            var id = $(this).attr('id').replace('deleteForm', ''); // Mengambil id dari form
            var form = $(this); // Menyimpan referensi form

            Swal.fire({
                title: 'Apakah Anda yakin?',
                text: "Anda tidak akan dapat mengembalikan ini!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#d33',
                cancelButtonColor: '#3085d6',
                confirmButtonText: 'Ya, Hapus Data!',
                cancelButtonText: 'Batal'
            }).then((result) => {
                if (result.isConfirmed) {
                    // Jika pengguna mengonfirmasi penghapusan, kirimkan permintaan penghapusan ke server
                    $.ajax({
                        type: "POST",
                        url: "/mitra/" + id,
                        data: {
                            _token: "{{ csrf_token() }}",
                            id: id
                        },
                        success: function(response) {
                            // Periksa apakah respons berisi pesan kesalahan atau berhasil
                            if (response.error) {
                                Swal.fire({
                                    icon: 'error',
                                    title: 'Oops...',
                                    text: response.error
                                });
                            } else {
                                Swal.fire({
                                    title: "Sukses !",
                                    text: "Data berhasil dihapus!",
                                    icon: "success"
                                }).then((result) => {
                                    location.reload();
                                });
                            }
                        },
                        error: function(error) {
                            Swal.fire({
                                icon: 'error',
                                title: 'Oops...',
                                text: "Terjadi Kesalahan!"
                            });
                        }
                    });
                }
            });
        });
    });
</script>
@endpush